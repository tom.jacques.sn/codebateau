const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const port = 8082;
const mqtt = require('mqtt');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Créer une connexion à la base de données
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'Bateau',
});

const mqttLocal = 'mqtt://localhost';
const topicLocal = "App";

function messageMqtt(){
    const client = mqtt.connect(mqttLocal);

    client.on('connect', () => {
        console.log(`Connecté au broker MQTT sur ${mqttLocal}`);
        client.subscribe(topicLocal); // S'abonne au sujet "topic"
    });
    
    client.on('message', (topic, message) => {
        const messageString = message.toString();
        console.log("Message reçu sur MQTT : " + messageString);
     
        // Diviser le message en fonction de ":"
        const messageParts = messageString.split(':');
        
        // Vérifier si le message a le bon format
        if (messageParts.length === 4) {
            // Extraire les valeurs de latitude, longitude, cap et vitesse du vent pour chaque bateau
            const boat1Data = messageParts[0].split(',');
            const boat2Data = messageParts[1].split(',');
            const windSpeed = parseFloat(messageParts[3]);
            const collisionDetected = messageParts[2].toLowerCase() === 'true';
        
            console.log('Collision détectée : ', collisionDetected);
            // Mettre à jour les données dans la base de données MySQL pour le premier bateau
            const queryBoat1 = `UPDATE bateaux SET position_lat = ?, position_lng = ?, cap = ? WHERE id = 1`;
            db.query(queryBoat1, boat1Data, (err, result) => {
                if (err) {
                    console.error('Erreur lors de la mise à jour des données du premier bateau dans la base de données:', err);
                } else {
                    console.log('Données du premier bateau mises à jour avec succès dans la base de données');
                }
            });
    
            // Mettre à jour les données dans la base de données MySQL pour le deuxième bateau
            const queryBoat2 = `UPDATE bateaux SET position_lat = ?, position_lng = ?, cap = ? WHERE id = 2`;
            db.query(queryBoat2, boat2Data, (err, result) => {
                if (err) {
                    console.error('Erreur lors de la mise à jour des données du deuxième bateau dans la base de données:', err);
                } else {
                    console.log('Données du deuxième bateau mises à jour avec succès dans la base de données');
                }
            });

            // Mettre à jour les données dans la table collisions
            const query = 'UPDATE collisions SET collision_detected = ? WHERE id = 1';
            db.query(query, [collisionDetected], (err, result) => {
                if (err) {
                    console.error('Erreur lors de la mise à jour des données de collision dans la base de données:', err);
                } else {
                    console.log('Données de collision mises à jour avec succès dans la base de données');
                }
            });
   
        } else {
            console.error("Le message MQTT n'a pas le bon format");
        } 
    });
}

// Connexion à la base de données
db.connect((err) => {
    if (err) {
        console.error('Erreur de connexion à la base de données:', err);
    } else {
        console.log('Connecté à la base de données');
    }
});

app.get('/bateaux', (req, res) => {
    const query = 'SELECT * FROM bateaux';

    db.query(query, (err, results, fields) => {
        if (err) {
            console.error('Erreur lors de la récupération des données des bateaux:', err);
            res.status(500).send('Erreur serveur');
        } else {
            console.log('Envoie des données:', results );
            res.json(results );
        }
    });
});

app.listen(port, () => {
    console.log(`Serveur écoutant sur le port ${port}`);
});

messageMqtt();
